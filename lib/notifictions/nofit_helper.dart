import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:push_notification/notifictions/firebase_messaging.dart';
// import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

const MethodChannel platform = MethodChannel(
  'dexterx.dev/flutter_local_notifications_example',
);

Future<void> initNotifications(
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin,) async {
  var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
  var initializationSettingsIOS = IOSInitializationSettings(
    requestAlertPermission: false,
    requestBadgePermission: false,
    requestSoundPermission: false,
    onDidReceiveLocalNotification: (id, title, body, payload) async {
      // didReceiveLocalNotificationSubject.add(ReminderNotification(
      //     id: id, title: title, body: body, payload: payload));
    },
  );

  var initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS,
  );

  await flutterLocalNotificationsPlugin.initialize(
    initializationSettings,
    onSelectNotification: (payload) async {
      if (payload != null) {
        debugPrint('notification payload: ' + payload);
      }
      // selectNotificationSubject.add(payload);
    },
  );
  // await _configureLocalTimeZone();
}

// Future<void> _configureLocalTimeZone() async {
//   tz.initializeTimeZones();
//   final timezone = await FlutterNativeTimezone.getLocalTimezone();
//   tz.setLocalLocation(tz.getLocation(timezone));
// }

void requestIOSPermissions(FlutterLocalNotificationsPlugin notifPlugin) {
  var iosLocalPlugin = notifPlugin.resolvePlatformSpecificImplementation<
      IOSFlutterLocalNotificationsPlugin>();

  iosLocalPlugin?.requestPermissions(
    alert: true,
    badge: true,
    sound: true,
  );
}

Future<void> scheduleNotification(
  String id,
  String body,
  DateTime schedule,
) async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
    id,
    'Reminder notifications',
    'Remember about it',
  );
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
    android: androidPlatformChannelSpecifics,
    iOS: iOSPlatformChannelSpecifics,
  );

  final zonedDateTime = tz.TZDateTime.from(schedule, tz.local).add(
    const Duration(seconds: 5),
  );

  await notifPlugin.zonedSchedule(
    0,
    'Reminder',
    body,
    zonedDateTime,
    platformChannelSpecifics,
    androidAllowWhileIdle: true,
    uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.wallClockTime,
  );
}
