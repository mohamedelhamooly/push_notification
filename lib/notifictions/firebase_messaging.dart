import 'dart:convert';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final notifPlugin = FlutterLocalNotificationsPlugin();
NotificationAppLaunchDetails notifAppLaunchDtls;

const channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.max,
);

Future<void> registerNotification() async {
  await Firebase.initializeApp();

  final messaging = FirebaseMessaging.instance;

  await messaging.requestPermission();

  await messaging.setForegroundNotificationPresentationOptions(
    alert: true, // Required to display a heads up notification
    badge: true,
    sound: true,
  );
  var initialMessage = await messaging.getInitialMessage();
  // If the message also contains a data property with a "type" of "chat",
  // navigate to a chat screen
  if (initialMessage != null) {
    print("");
  }
  String token = await messaging.getToken();
  print("The token is "+token);

  FirebaseMessaging.onMessage.listen(
    (message) {
      final notification = message.notification;
      final android = message.notification?.android;
      if (notification != null && android != null) {
        notifPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              // icon: android?.smallIcon,

              // other properties...
            ),
          ),
          payload: jsonEncode(message.data),
        );
        print('Message also contained a notification: ${message.notification}');
      }
    },
  );
  if (Platform.isAndroid) {
    await notifPlugin
        .resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    var initializationSettingsAndroid = AndroidInitializationSettings("ic_launcher");
    var initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
    );
    await notifPlugin.initialize(
      initializationSettings,
      onSelectNotification: (payload) {
        // _handleMessageForAndroid(jsonDecode(payload));
        return;
      },
    );
  }

}
